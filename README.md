# Payrent Database Dump Script
  
  Helper script for downloading Payrent database backups. 

### Disclaimer

  This script has only been tested on linux systems and works ONLY with the current Payrent (2022) mysql database setup.

  If you're on a different OS, I'm so sorry.

### Dependencies

  1. mysql-client
  2. pv

  ```
  sudo apt install mysql-client pv -y
  ```

  3. Setting up your `mysql` configuration

  You will need to save your configuration through `mysql_config_editor`.

  The basic command is:

  ```bash
  mysql_config_editor set --login-path=payrent-dev --host=xxx.rds.amazonaws.com --user=root --password
  ```

  You will also want to add one for `payrent-stage` and `payrent-live`.

  If you need more information about `mysql_config_editor` you can check their [docs][1].

  [1]: https://dev.mysql.com/doc/refman/8.0/en/mysql-config-editor.html

### Installation

  To install, just clone this repo.

  ```bash
  $ git clone git@gitlab.com:reilg/db-backup.git
  ```

### Usage

  To use this script just cd into the project directory, call `./dump` and throw in a few flags.

  Flags:
  ```bash
  -l  login     your mysql login-path name. default: "payrent-dev"
  -d  database  database name. default: "payrent"
  -c  clean     cleanup your storage folder. deletes backup files that are more than 90 days old.
  ```

  Example:
  ```bash
  $ ./dump -s payrent-live
  ```

### Default Folder Path 

  If you want to setup a different default folder, you can set the `$PD_FOLDER` environment variable.

  Add this to your `.bashrc` or `.zshrc` file and reload your terminal.

  ```base
  export PD_FOLDER="$HOME/Documents/payrent-backups"
  ```

  Now all your backups will appear in that folder. 
